//
//  ViewController.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 29/08/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var tableViewIDs: UITableView!
    
    private var arrayID = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Retira as celulas que estão a mais da tableView
        tableViewIDs.tableFooterView = UIView()
        //Faz a requisição GET ao servidor tendo como resposta a lista de IDs
        Alamofire.request("http://dev.4all.com:3003/tarefa/").responseJSON { (response) in
            //responseValue é um dicionário do tipo String:Any que contém JSON
            if let responseValue = response.result.value as! [String: Any]? {
                //Instâcia do objeto ID, no qual, é responsável em converter o dicionário em um array de Strings
                let id = ID(responseValue)
                //A função getIDs() retorna um array de Strings, no qual contém a lista com os IDs
                self.arrayID = id.getIDs()
                //A função reloadData() atualiza os valores da tabela (tableViewIDs)
                self.tableViewIDs.reloadData()
            }
        }
    }
}

//As funções abaixo são responsáveis por popularem a lista interativa de IDs (TableView)
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    //Essa função retorna a quantidade de linhas da lista para a criação da tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayID.count
    }
    
    //Essa função retorna a célula da tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Instancia uma célula da tableView com os seus atributos, no caso é uma label em CellTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CellTableViewCell
        //Atribui à label os IDs
        cell?.LabelID.text = arrayID[indexPath.row]
        return cell!
    }
    
    //Essa função é responsável por atribuir um evento à célula selecionada
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Faz com que a célula fique desselecionada quando voltar à view inicial (Tela Inicia)
        tableView.deselectRow(at: indexPath, animated: true)
        //Cria a instância do PrincipalViewController (Tela Principal)
        let principalVC = storyboard?.instantiateViewController(withIdentifier: "PrincipalViewController") as? PrincipalViewController
        //A função setID() tem como objetivo fazer a passagem do ID selecionado na tableView para a Tela Principal
        principalVC?.setID(id: arrayID[indexPath.row])
        //Chama a view PrincipalViewController
        self.navigationController?.pushViewController(principalVC!, animated: true)
    }
}


