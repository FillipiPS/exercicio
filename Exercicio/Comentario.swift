//
//  Comentario.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 31/08/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import Foundation

class Comentario {
    
    let urlFotoC: String
    let nome: String
    let nota: Int
    let tituloC: String
    let comentario: String
    
    init(urlFotoC: String, nome: String, nota: Int, tituloC: String, comentario: String) {
        self.urlFotoC = urlFotoC
        self.nome = nome
        self.nota = nota
        self.tituloC = tituloC
        self.comentario = comentario
    }
    
}
