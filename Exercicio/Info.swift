//
//  Info.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 31/08/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import Foundation

struct Info {
    let cidade: String
    let urlFoto: String
    let urlLogo: String
    let titulo: String
    let telefone: String
    let texto: String
    let endereco: String
    let latitude: Double
    let longitude: Double
    let bairro: String
    var comentarios = [Comentario]()
}



