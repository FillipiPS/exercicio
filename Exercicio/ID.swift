//
//  ID.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 29/08/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import Foundation

class ID {
    
    private let ids : [String]
    
    //Inicializador da classe, recebe o dicionário e converte para um array de Strings, caso for nulo é atribuido um array vazio
    init(_ dictionary: [String: Any]) {
        self.ids = dictionary["lista"] as? [String] ?? [""]
    }
    
    //Retorna o array com os IDs
    func getIDs() -> [String] {
        return ids
    }
    
}
