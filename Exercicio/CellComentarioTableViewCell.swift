//
//  CellComentarioTableViewCell.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 01/09/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import UIKit

class CellComentarioTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewFoto: UIImageView!
    @IBOutlet weak var labelNome: UILabel!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var textViewComentario: UITextView!
    @IBOutlet weak var labelNota: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
