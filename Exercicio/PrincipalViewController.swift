//
//  PrincipalViewController.swift
//  Exercicio
//
//  Created by Fillipi Paiva Suszek on 29/08/19.
//  Copyright © 2019 Fillipi Paiva Suszek. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class PrincipalViewController: UIViewController {
    
    //Componentes da View
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewFoto: UIImageView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textViewTexto: UITextView!
    @IBOutlet weak var labelEndereco: UILabel!
    @IBOutlet weak var imageViewPin: UIImageView!
    @IBOutlet weak var tableViewComentario: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    //Atributos do Info
    private var id: String = ""
    private var numero: String = ""
    private var endereco: String = ""
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var cidade: String = ""
    private var bairro: String = ""
    private var imageFoto: UIImage? = nil
    private var arrayComentario: [Comentario] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Essa linha de código tem como objetivo tirar o título Back (Voltar) do botão voltar
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //A função setRounded() faz com que as ImageViews para Logo e Pin (ImageView abaixo do mapa) possuírem aspecto arrendondado
        self.imageViewLogo.setRounded()
        self.imageViewPin.setRounded()
        
        //Faz uma requisição GET ao servidor tendo como parâmetro o id obtido atravéz da seleção da View anterior
        Alamofire.request("http://dev.4all.com:3003/tarefa/\(id)").responseJSON { (response) in
            if let responseValue = response.result.value as! [String: Any]? {
                //É criada uma thread com o objetivo de atualizar os componentes da View
                DispatchQueue.main.async() {
                    self.startAtrib(responseValue: responseValue)
                }
            }
        }
    }
    
    //Evento ao pressionar o botão Ligar, que tem como funcionalidade realizar uma ligação para o número obtido na requisão ao servidor
    @IBAction func ligarButton(_ sender: UIButton) {
        //Condição caso não tiver nenhum número manda uma mensagem estilo Pop Up informando, caso o contrário realiza a ligação
        if self.numero.isEmpty {
            
            //Chama a função popUp()
            popUp(titulo: "Sem número", mensagem: "Nehum numero cadastrado.")
        } else {
            
            //Monta uma URL contendo o número do telefone
            guard let number = URL(string: "tel://" + self.numero) else { return }
            
            //Realiza a ligação
            UIApplication.shared.open(number)
        }
    }
    
    //Evento ao pressionar o botão Endereço, que tem como funcionalidade mostrar uma mensagem estilo Pop Up com o endereço
    @IBAction func enderecoButton(_ sender: UIButton) {
        //Chama a função popUp()
        popUp(titulo: "Endereço", mensagem: self.endereco)
    }
    
    //Evento ao pressionar o botão Comentário, que tem como funcionalidade fazer um scroll para a Table View com os comentários
    @IBAction func comentarioButton(_ sender: UIButton) {
        //A função setContentOffset(), da scrollView, faz com que o componente inserido nela seja a origem na tela, passando como parâmetro o X e Y do componente
        scrollView.setContentOffset(CGPoint(x: tableViewComentario.frame.origin.x, y: tableViewComentario.frame.origin.y), animated: true)
    }
    
    //Essa função é responssável por receber um dicionário JSON, da requisição, e atribuir aos atributos
    func startAtrib(responseValue: [String: Any]) {
        //É cirada uma instância de Info
        let inf = Info.init(json: responseValue)
        //É criada uma thread com o objetivo de atualizar as imagens
        DispatchQueue.main.async() {
            self.imageViewFoto.image = self.getImage(from: inf!.urlFoto)
            self.imageViewLogo.image = self.getImage(from: inf!.urlLogo)
        }
        
        //Atribuições dos componentes
        self.numero = inf?.telefone ?? ""
        self.endereco = inf?.endereco ?? ""
        
        self.latitude = inf?.latitude ?? 0.0
        self.longitude = inf?.longitude ?? 0.0
        //Chama a função startMap()
        self.startMap()
        
        self.cidade = inf?.cidade ?? ""
        self.bairro = inf?.bairro ?? ""
        //Edita o título da tela com os atributos cidade e bairro
        self.title = "\(self.cidade) - \(self.bairro)"
        
        self.labelTitulo.text = inf?.titulo
        self.textViewTexto.text = inf?.texto
        self.labelEndereco.text = inf?.endereco
        self.arrayComentario = inf!.comentarios
        
        //A função getIDs() retorna um array de Strings, no qual contém a lista com os IDs
        self.tableViewComentario.tableFooterView = UIView()
        
        //Chama a função atualizaTableView()
        atualizaTableView()
        
        //Atualiza a TableView
        self.tableViewComentario.reloadData()
        
    }
    
    //É responsável por iniciar o mapa com o marcador
    func startMap() {
        //Cria uma referencia de MKPointAnnotation
        let annotation = MKPointAnnotation()
        
        //Cria uma coordenada com as variáveis latitude e longitude adquiridas através da requisição GET ao servidor
        annotation.coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        
        //É criada uma delimitação da visualização do mapa, ou seja, é feita uma aproximação do mapa
        let initialregion = MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001))
        
        //É atribuida a delimitação
        self.mapView.setRegion(initialregion, animated: false)
        
        //É atribuido o marcador com as coordenadas
        self.mapView.addAnnotation(annotation)
    }
    
    //A função é responsável por aumentar a altura da TableView, quando o botão Comentários for pressionado os comentários ficarem em tela cheia
    func atualizaTableView() {
        if self.arrayComentario.count > 0 {
            self.tableViewHeight.constant = CGFloat(self.arrayComentario.count * 160)
        }
    }
    
    //Essa função recebe uma String em formato URL e retorna uma UIImage
    func getImage(from string: String) -> UIImage? {
        //Monta uma URL
        guard let url = URL(string: string)
            else {
                return nil
        }
        
        var image: UIImage? = nil
        do {
            //Obtém a imagem
            let data = try Data(contentsOf: url, options: [])
            
            //Monta a imagem
            image = UIImage(data: data)
        }
        catch {
            print(error.localizedDescription)
        }
        
        return image
    }

    //Função responsável por emitir uma menssagem estilo Pop Up, com o Título, Mensagem e um botão (OK)
    func popUp(titulo: String, mensagem: String){
        let alert = UIAlertController(title: titulo, message: mensagem, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Essa função recebe o ID da view anterior e atribui para a variável id nesta View
    func setID(id: String) {
        self.id = id
    }

}


extension Info {
    init?(json: [String: Any]) {
        //Faz a atribuição dos campos JSON para as constanstes locais
        guard let cidade = json["cidade"] as? String,
            let urlFoto = json["urlFoto"] as? String,
            let urlLogo = json["urlLogo"] as? String,
            let titulo = json["titulo"] as? String,
            let telefone = json["telefone"] as? String,
            let texto = json["texto"] as? String,
            let endereco = json["endereco"] as? String,
            let latitude = json["latitude"] as? Double,
            let longitude = json["longitude"] as? Double,
            let bairro = json["bairro"] as? String,
            let comentariosJSON = json["comentarios"] as? [[String: Any]]
        else {
            return nil
        }
        
        //Por ser um array de comentários é feita a varedura em comentariosJSON
        for cj in comentariosJSON {
            guard let urlFotoC = cj["urlFoto"] as? String,
                let nome = cj["nome"] as? String,
                let nota = cj["nota"] as? Int,
                let tituloC = cj["titulo"] as? String,
                let comentario = cj["comentario"] as? String
            else {
                return nil
            }
            
            //Inicialização do objeto Comentário, contendo os valores
            let c = Comentario(urlFotoC: urlFotoC, nome: nome, nota: nota, tituloC: tituloC, comentario: comentario)
            
            //Faz uma inserção (append) no array comentarios, no qual, é utilizado para popular a tableView de comentários
            self.comentarios.append(c)
            
        }

        //Atribuição para as variáveis que são responsáveis por mastrar as informações na tela
        self.cidade = cidade
        self.bairro = bairro
        self.urlFoto = urlFoto
        self.urlLogo = urlLogo
        self.titulo = titulo
        self.telefone = telefone
        self.texto = texto
        self.endereco = endereco
        self.latitude = latitude
        self.longitude = longitude
        
    }
}

extension UIImageView {
    
    //A função setRounded() faz com que a ImageView tenham um aspecto arrendondado
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
    
}

//As funções tableView são responsáveis por popularem a lista de Comentários
extension PrincipalViewController: UITableViewDelegate, UITableViewDataSource {
    
    //Essa função retorna a quantidade de linhas da lista para a criação da tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //Se não tiver nehum comentário é adicionada uma Label com a informação que não há comentários
        if arrayComentario.count == 0 {
            tableViewComentario.emptyMessage(message: "Sem comentários", tableView: tableViewComentario)
        }
        return arrayComentario.count
    }

    //Essa função retorna a célula da tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Instancia uma célula da tableView com os seus atributos urlFoto, nome, titulo, comentario e nota
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? CellComentarioTableViewCell
        
        //A função setRounded() deixa a imageViewFoto com aspecto arendondado
        cell2?.imageViewFoto.setRounded()
        cell2?.imageViewFoto.image = self.getImage(from: arrayComentario[indexPath.row].urlFotoC)
        cell2?.labelNome.text = arrayComentario[indexPath.row].nome
        cell2?.labelTitulo.text = arrayComentario[indexPath.row].tituloC
        cell2?.textViewComentario.text = arrayComentario[indexPath.row].comentario
        cell2?.labelNota.text = ""
        
        //Recebe a nota, ou seja, a quantidade de estrelas
        let stars = arrayComentario[indexPath.row].nota
        
        //Lógica para mostrar a nota em estrelas preenchidas, por exemplo:
        //nota 2 = ★★☆☆☆
        for n in 1...5 {
            if n <= stars {
                cell2?.labelNota.text = (cell2?.labelNota.text)! + "★"
            } else {
                cell2?.labelNota.text = (cell2?.labelNota.text)! + "☆"
            }
        }
        return cell2!
    }
    
}

extension UITableView {
    
    //Adiciona uma label com uma mensagemm à tableView
    func emptyMessage(message: String, tableView: UITableView) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor(red:0.80, green:0.54, blue:0.10, alpha:1.0)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "System", size: 15)
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = .none;
        
    }
}
